
public class DimensionException extends Exception{

	public DimensionException() //default constructor for error message
	{
		super("Invalid Dimensions"); //error message when length <= 0 
	}
	public DimensionException(String aMsg)
	{
		super(aMsg);
	}
	
}
