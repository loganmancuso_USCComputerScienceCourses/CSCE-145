import java.util.Scanner;

public class DateConverterBackend {
	
	public void MonthError(String date) 
			throws MonthException
	{
		try 
		{
			String[] splitDate = date.split("/"); //split at / mark
			String theMonth = splitDate[0]; 
			int month = Integer.parseInt(theMonth);

			//month out of bounds 1-12
			if (month > 12 || month < 1)
			{
				throw new MonthException();
			} 
		}
		catch (MonthException e)
		{
			System.out.println(e.getMessage());
		}
	}
	public void monthChecking(int month, int day) 
			throws DayException
	{
		try
		{
			//for months with 29 days
			if (month==2)
			{
				if (day<29 && day>1)
				{
					System.out.println("Febuary "+ day);
				}
				else 
				{
					throw new DayException();
				}
			}
			//for months with 30 days
			if (month==4 || month==6 || month==9 || month==11)
			{
				if (day<30 && day>1) //check day matches month
				{
					switch (month)
					{
					case 4:
						System.out.println("April "+ day);
						break;
					case 6:
						System.out.println("June "+ day);
						break;
					case 9: 
						System.out.println("September "+ day);
						break;
					case 11:
						System.out.println("November "+ day);
						break;
					}
				}
				else 
				{
					throw new DayException();
				}
			}
			//for months with 31 days
			if (month==1 || month==3 || month==5 || month==7 
					|| month==8 || month==10 || month==12)
			{
				if (day<31 && day>1) //check day matches month
				{
					switch (month)
					{
					case 1:
						System.out.println("January "+ day);
						break;
					case 3:
						System.out.println("March "+ day);
						break;
					case 5: 
						System.out.println("May "+ day);
						break;
					case 7:
						System.out.println("July "+ day);
						break;
					case 8:
						System.out.println("August "+ day);
						break;
					case 10:
						System.out.println("October "+ day);
						break;
					case 12:
						System.out.println("December "+ day);
						break;
					}
				}
				else 
				{
					throw new DayException();
				}
			}
		}
		catch (DayException e)
		{
			System.out.println(e.getMessage());	
		}
	}
}
