/*Mancuso, Logan 
 * This program will take a list of 10 taco's and sort them 
 * based on price from lowest to highest and then output them to the user
 */

import java.util.Scanner;
import java.util.Arrays;

public class tacoSorter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter the list of Taco's and the prices \n"
				+ "associated with each\n");
		//Initialize arrays 
		String [] arrayTacoNames = new String [10];
		double [] arrayTacoPrices = new double [10];
		//begin populating array
		for (int i=0; i<10; i++)
		{
			Scanner keyboard = new Scanner(System.in);
			System.out.println("Enter the Taco Name for Taco # " +(i+1));
			String tacoName = keyboard.nextLine();
			arrayTacoNames[i] = tacoName; //array of the names
			for (int j=0; j<arrayTacoPrices.length; j++)
			{
				System.out.println("Enter the cost of the Taco");
				double tacoPrice = keyboard.nextDouble();
				arrayTacoPrices[i] = tacoPrice; //array of the cost for each Taco 
				break;
				//pull out of cost loop to go back to name loop
			}
		}
		//Sort the array and initialize variables 
		double temp;
		int first,j,i;
		String temp2;
		for (i=arrayTacoPrices.length-1; i>0; i--)
		{
			first = 0;
			for (j=1; j<=i; j++)
			{
				if (arrayTacoPrices[j] > arrayTacoPrices[first])
					first = j;
			}
			//swap the value of the larger int and swap indices with greater int
			temp = arrayTacoPrices[first];
			arrayTacoPrices[first] = arrayTacoPrices[i];
			arrayTacoPrices[i] = temp;
			//do the same with the name of the taco that corresponds to that price value
			temp2 = arrayTacoNames[first];
			arrayTacoNames[first] = arrayTacoNames[i];
			arrayTacoNames[i] = temp2;
		}
			
		//display menu for taco's
		System.out.println("The Taco Menu\n");
		for (i=0; i<10; i++)
		{
			System.out.println("The Taco " + arrayTacoNames[i] + " at cost " + arrayTacoPrices[i] + " dollars");
		}
	}

}
