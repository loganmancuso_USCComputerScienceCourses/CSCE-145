/*Mancuso, Logan	
 * This program will find the average temperature for 10 days
 * then sort the list of temperatures that were above the average
 * and out put them to the user 
 */
import java.util.Arrays;
import java.util.Scanner;
public class aboveAverageTemperature {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("This is the above average temperature program \n"
				+ "Enter 10 different temparatures");
		Scanner keyboard = new Scanner(System.in);
		//array to hold values
		double [] arrayTemperatures = new double [10];
		//create a for loop to take inputs and populate the array
		for (int i=0; i<arrayTemperatures.length; i++)
		{
			System.out.println ("Enter the temperature of day "+ (i+1));
			int newTemperature = keyboard.nextInt();
			//check for invalid inputs ie negative numbers 
			if (newTemperature >= 0)
			{
				arrayTemperatures[i] = newTemperature;
			}
			else 
			{
				System.out.println("Invalid input, temperature not correct");
				continue;
			}
		}
			
		// take average of temperatures 
		double sum = 0;
		for (int i=0; i<arrayTemperatures.length; i++)
		{
			sum = sum + arrayTemperatures[i];
		}
			
		//calculate average 
		double average = sum/arrayTemperatures.length;
		System.out.println("\n The average is " + average +"\n");
			
		//copy array to store values of temperatures above average 
		double [] aboveTemperatures = new double [arrayTemperatures.length];
		for (int i=0; i<arrayTemperatures.length; i++)
		{
			aboveTemperatures[i] = arrayTemperatures[i];
		}
						
		//find values within string greater than average and assign rest as zero 
		for (int i=0; i<aboveTemperatures.length; i++)
		{
			if (aboveTemperatures[i] > average)
			{	
				aboveTemperatures[i] = aboveTemperatures[i];
			}
			else 
			{
				aboveTemperatures[i] = 0;
			}
		}	
		
		//sort the new array of values 
		Arrays.sort(aboveTemperatures);
		System.out.println("Days above the average are \n");	
		for (int i=0; i< aboveTemperatures.length; i++)
		{
			if (aboveTemperatures[i] > 0)
			{
				System.out.println("Day " + (i+1) + " with temperature " + aboveTemperatures[i]);
			}
		}
	}

}
