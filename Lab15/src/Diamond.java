
public class Diamond extends ShapeBasics implements DiamondInterface{
	
	private int width;
	
	//default constructor
	public Diamond()
	{
		super();
		width = 0;
	}
	//paramaterized constructor
	public Diamond(int theWidth, int Offset)
	{
		super(Offset);
		width = theWidth;
	}
	//accessor
	public int getWidth ()
	{
		return this.width;//call to mutator for error checking 
	}
	//mutator 
	public void setWidth (int newWidth) 
	{
		if (width>0 && (width%2!=0)) //if an odd number set it 
		{
			this.width = newWidth;
		}
		else //if not set it -1
		{
			this.width = (newWidth--);
		}
			
	}
	//static method
	private static void skipSpaces(int number)//set skipSpaces 
	{
		for (int count = 0; count < number; count++)
			System.out.print(" ");
	}
	
	//other methods
	public void drawHere()
	{
		drawTopV();
		drawBottomV();	
	}
	
	private void drawTopV()
	{
		int startOfLine = getOffset()+width/2; //setting the first line
		skipSpaces(startOfLine);
		System.out.println("*"); //printing top of V
		int lineCount = width/2; 
		int insideWidth = 1;
		for (int count=0; count<lineCount; count++)
		{
			startOfLine--;
            skipSpaces(startOfLine);
            System.out.print('*');
            skipSpaces(insideWidth);
            System.out.println('*');
            insideWidth = insideWidth + 2;
		}
	}
	private void drawBottomV()
	{
		int startOfLine = getOffset();//sets offset of starting line by user input 
		int lineCount = width/2-1; //counting backwards from line count 
		int insideWidth = lineCount*2-1; //set width equal to line count (association is linecount*2-1=userinput width)
		for (int count=0; count<lineCount; count++)
		{
			startOfLine ++;
			skipSpaces(startOfLine);
			System.out.print('*');
			skipSpaces(insideWidth);
			System.out.println('*');
			insideWidth = insideWidth - 2;
		}
		skipSpaces(getOffset()+width/2);
		System.out.println("*"); //bottom of V
	}
	
}
