/*
 * Written by JJ Shepherd
 * 
 */
public class lab12Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Creates a new instance of a grader
		grader g = new grader();
		//Collects the grade information
		g.setquizOne();
		g.setquizTwo();
		g.setmidtermExam();
		g.setfinalExam();
		//Calculates the final grade
		g.calculateFinalGrade();
		//Prints the results to the user
		g.printFinalGrade();
	}

}
