
public class Employee extends SimplePerson{

	//instance variables 
	private double annualSalary;
	private String hireDate;
	private int idNumber;
	
	//constructors without parameter 
	public Employee()
	{
		super();
		this.annualSalary = 0.0;
		this.hireDate = null;
		this.idNumber = 0;	
	}
	//constructor with parameter
	public Employee(String aName, double anAnnualSalary, String aHireDate, int anIDNumber)
	{
		super(aName);
		this.annualSalary = anAnnualSalary;
		this.hireDate = aHireDate;
		this.idNumber = anIDNumber;	
	}
	
	//accessors 
	public double getAnnualSalary()
	{
		return this.annualSalary;
	}
	public String getHireDate()
	{
		return this.hireDate;
	}
	public int getIDNumber()
	{
		return this.idNumber;
	}
	
	//mutators
	public void setAnnualSalary(double anAnnualSalary)
	{
		if (annualSalary > 0)
		{
			this.annualSalary = anAnnualSalary;
		}
		else 
		{
			this.annualSalary = 0;
		}
	}
	public void setHireDate(String aHireDate)
	{
		if (hireDate != null)//check for valid date
		{
			this.hireDate = aHireDate;
		}
		else 
		{
			this.hireDate = "Invalid Date";
		}
	}
	public void setIDNumber(int anIDNumber)
	{
		if (idNumber >0)
		{
			this.idNumber = anIDNumber;
		}
		else 
		{
			this.idNumber = 0;
		}
		
	}
	
	//other methods
	public void writeOutput()
	{
		super.writeOutput();
		System.out.println("Annual Salary: "+annualSalary+ 
				"\nHire Date: "+ hireDate+ 
				"\nID Number: "+ idNumber);
	}
	//equals
	public boolean equals(Employee otherEmployee) 
	{
		return this.hasSameName //same name call to SimplePerson
		(otherEmployee)&& (this.annualSalary == otherEmployee.annualSalary)//same annual salary
		&& (this.hireDate.equals(otherEmployee.hireDate)) //same hire date
		&& (this.idNumber==otherEmployee.idNumber); //same ID number
	}
	
}
