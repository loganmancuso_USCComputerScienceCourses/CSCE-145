
public class SimplePerson {

	//instance variables 
	private String Name;
	
	//constructor with no parameter
	public SimplePerson()
	{
		this.Name = "No name yet";
	}
	
	//constructor with parameter
	public SimplePerson(String aName)
	{
		this.Name= aName;
	}
	
	//accessor
	public String getName()
	{
		return this.Name;
	}
	
	//mutator
	public void setName(String aName)
	{
		if (aName != null)//TODO check for valid inputs
		{
			this.Name= aName;
		}
		else 
		{
			this.Name = "Invalid input";
		}
	}
	
	//other methods	
	public void writeOutput()
	{
		System.out.println("Name: "+ Name);
	}
	//equals check for similarities
	public boolean equals(SimplePerson otherPerson)
	{
		return this.Name.equalsIgnoreCase(otherPerson.Name);
	}
	public boolean hasSameName(SimplePerson otherPerson)
	{
		return this.Name.equalsIgnoreCase(otherPerson.Name); // has same name 
	}
	
}
