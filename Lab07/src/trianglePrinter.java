/*Mancuso, Logan 
 * This program will take a user input and print a 
 * Triangle made of '*' with the inputed height
 */
import java.util.Scanner;

public class trianglePrinter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//variable for continuing if 0, 1 or negative is inputed 
		String cont = ""; 
		Scanner keyboard = new Scanner(System.in);
		do
		{
			System.out.println("This program will print a triangle with a given height, \n"
					+ "how high do you want the triangle?");
			int height = keyboard.nextInt();
			// check for negative, 0, and 1 as inputs  
				if (height > 1)
				{
					//create triangle 
					// increasing *'s going down 
					for (int a = 0; a < height; a++) // loop for x time of height inputed
					{
						for (int b = 0; b<a; b++)
						{
							System.out.print("*");
						}
						System.out.println(" ");
					}
					//decreasing *'s going down for second half of triangle 
					for (int c = height; c >= 0; --c ) // loop for x-1 times of height inputed 
					{
						for (int d = c; d >= 1; --d)
						{
							System.out.print("*");
						}
						System.out.println(" ");
					}
					// triangle loop completed 
					break; //once valid input is met program terminates 
				} 
				else //if user inputs a 0, 1 or negative
				{
					//prompt user for a restart
					System.out.println("invalid height want to try again? yes or no ");
					cont = keyboard.next();
				}
			//while loop for retry after first input is wrong
		} while (cont.equalsIgnoreCase("yes"));
				
		System.exit(0); //if user inputs other than y or yes system terminates
	}

}
