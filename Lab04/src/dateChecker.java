/*
 *  Mancuso, Logan this program will evaluate the validity of the inputed date. 
 *  Given a date in mm/dd form  i will store the mm in month and dd in day 
 *  i will have to check the month as a valid input from 1 to 12 
 *  then use the month to determine if the day is valid. 
 *  i will also have to have an else statement that outputs "invalid date" if the prompted user
 *  inputs a wrong date.
 */
import java.util.Scanner;	
public class dateChecker {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Please input a date in mm/dd format.");
		Scanner keyboard = new Scanner(System.in);
		String inputDate = keyboard.next();
		//now to split up the string at the forward slash 
			String[] seperate = inputDate.split("/");
			String month = seperate[0];
			String day = seperate[1];
		//convert string to integer in order to evaluate;
			int theMonth = Integer.parseInt(month);
			int theDay = Integer.parseInt(day);
		/*
		 * create if else statement to evaluate validity of each input
		 * first section is for months with 1 to 31 days
		 * section two is for months with 1 to 30 days 
		 * the last section is for February 
		 */
			if ((theMonth == 1 || theMonth == 3 || theMonth == 5 || theMonth == 7 
					|| theMonth == 8 || theMonth == 10 || theMonth == 12)//these are months with 31 days
					&& ((theDay >=1 && theDay<=31)) // this checks that the corresponding months have a day that is between 1 and 31
				|| ((theMonth == 4 || theMonth == 6 || theMonth == 9 || theMonth == 11) //these are months with 30 days
					&& ((theDay >= 1 && theDay<= 30))) //this checks that the corresponding months have a day between 1 and 30
				|| (((theMonth == 2) // this is the month February with only 28 days in a non leap year
					&& ((theDay>=1 && theDay<=28))))){ //this checks that the day corresponding to February has between 1 and 28 days 
				
				System.out.println("Valid Date");
				
				}
			else
				System.out.println("Not a valid date");
	}

}
