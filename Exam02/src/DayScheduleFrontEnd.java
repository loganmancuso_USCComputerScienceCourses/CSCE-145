import java.util.Scanner;


public class DayScheduleFrontEnd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DailySchedule newSchedule = new DailySchedule();
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Welcome to the day planner system\n");
		boolean quit = false;
		while (quit==false)
		{
			System.out.println("Enter 1 to add an activiy\n"+
					"Enter 2 to remove an activity \n"+
					"Enter 9 to quit");
			int selection = keyboard.nextInt();
			keyboard.nextLine();
			
			switch (selection)
			{
			case 1:
				System.out.println("Enter name of the activity");
				String name = keyboard.nextLine();
				System.out.println("Enter starting time (in military format 0-23) of activity");
				int startHour = keyboard.nextInt();
				System.out.println("Enter ending time (in military format 0-23) of activity");
				int endHour = keyboard.nextInt();
				newSchedule.addActivity(new Activity(name,startHour,endHour)); 
				newSchedule.sortByTime();
				newSchedule.printActivities();
				break;
			case 2: 
				System.out.println("Enter name of the activity to remove");
				String aName = keyboard.nextLine();
				newSchedule.removeActivity(aName);
				newSchedule.printActivities();
				break;
			case 9:
				System.out.println("Bye");
				quit=true;
				break;
			default:
				System.out.println("invalid choice");
				newSchedule.printActivities();
				break;
			}
		}
	}

}
