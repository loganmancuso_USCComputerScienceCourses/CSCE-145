/*
 * Mancuso, Logan 
 * this program will be a game of rock, paper, scissors.
 * the user will input a choice and then the computer will 
 * Randomly generate a counter. it will be a best of 3 game
 */

import java.util.Scanner;
import java.util.Random;

public class rockPaperScissors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	// initialize variable exit from loop
		String quit = "";
	//initialize computer choice variable
		int compChoice; 
	//Beginning of loop for starting the game 
	do {
		//initialize user score as variable to record points 
				int userScore = 0; 
		//initialize computer score to record points
				int compScore = 0; 
		System.out.println("This is a game of rock, paper, scissors");
		Scanner keyboard = new Scanner(System.in);
		//create a while loop that loops 2 of 3 times  
			while (userScore <= 1 && compScore <= 1)
			{
				System.out.println("Please input your choice rock, paper, scissors");
					String userInput = keyboard.nextLine();
				//initialize computer random number variable
					Random numberGenerator = new Random (); 
				// have computer choose a random integer
					compChoice = numberGenerator.nextInt(3)+1;; 
						// have user input changed to integers and compare user to computer and score points 
								//user chooses rock
								if (userInput.equalsIgnoreCase("rock"))
								{
									int userChoice = 1;
									// comparing user to computer
										if (compChoice == 1)//rock
										{
											System.out.println("Its a tie!, no points awarded");
										}
										else if (compChoice == 2)//paper
										{
											System.out.println("Paper covers Rock, computer wins a point");
											compScore++;
											
										}
										else if (compChoice == 3)//scissors
										{
											System.out.println("Rock crushes Scissors, you win a point");
											userScore++;
											
										}
								}
								//user chooses paper
								else if (userInput.equalsIgnoreCase("paper"))
								{
									int userChoice = 2;
									// comparing user to computer
										if (compChoice == 1)//rock
										{
											System.out.println("Paper covers Rock, you win a point");
											userScore++;
											
										}
										else if (compChoice == 2)//paper
										{
											System.out.println("Its a tie!, no points awarded");
										}
										else if (compChoice == 3)//scissors
										{
											System.out.println("Scissors cuts Paper, computer wins a point");
											compScore++;
										}
								}
								//user chooses scissors
								else if (userInput.equalsIgnoreCase("scissors"))
								{
									int userChoice = 3;
									// comparing user to computer
										if (compChoice == 1)//rock
										{
											System.out.println("Rock crushes Scissors, computer wins a point");
											compScore++;
										}
										else if (compChoice == 2)//paper
										{
											System.out.println("Scissors cuts Paper, you win a point");
											userScore++;
										}
										else if (compChoice == 3)//scissors
										{
											System.out.println("Its a tie!, no points awarded");
										}
								}
								//if user inputs other than Rock, Paper, or Scissors
								else 
								{
									System.out.println("not a valid input computer scores!");
									compScore++;
									continue;
								}
						} //end of for loop 
		
						//declare a winner
						if (userScore > compScore)
						{
							System.out.println("You Won! \n" 
									+ "You had "+ userScore + "\n" 
									+ "Computer had " +compScore);
						}
						else if (userScore < compScore)
						{
							System.out.println("You Loose! \n" 
									+ "You had "+ userScore + "\n" 
									+ "Computer had " +compScore);
						}
						else if (userScore == compScore)
						{
							System.out.println("Its a tie!");
						}
				
			System.out.println("do you want to quit? yes or no");
				quit = keyboard.next();
				// if user inputs other than y system exits
			
		}
			while (!quit.equalsIgnoreCase ("yes"));
	
				System.exit(0); //if user inputs yes program terminates 
	
	}
}
